from django.shortcuts import render, redirect
from accounts.forms import LoginView, SignUpView
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User


def login_user(request):
    if request.method == "POST":
        form = LoginView(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("list_projects")
            else:
                form.add_error(None, "Incorrect Login Details")
    else:
        form = LoginView()
    context = {"form": form}
    return render(request, "accounts/login.html", context)


def logout_user(request):
    logout(request)
    return HttpResponseRedirect("/accounts/login/")


def user_signup(request):
    if request.method == "POST":
        form = SignUpView(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]

            if password == password_confirmation:
                user = User.objects.create_user(
                    username,
                    password=password,
                )
                login(request, user)
                return redirect("list_projects")
            else:
                form.add_error("password", "the passwords do not match")
    else:
        form = SignUpView()
    context = {
        "form": form,
    }
    return render(request, "accounts/signup.html", context)
