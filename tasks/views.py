from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.forms import ModelForm
from .models import Task


class TaskForm(ModelForm):
    class Meta:
        model = Task
        exclude = ["is_completed"]


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form = form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    return render(request, "tasks/create.html", {"form": form})


@login_required
def task_list(request):
    user = request.user
    tasks = Task.objects.filter(assignee=user)
    return render(request, "tasks/list.html", {"tasks": tasks})
