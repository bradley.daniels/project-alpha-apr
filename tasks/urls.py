from django.urls import path
from tasks import views


urlpatterns = [
    path("create/", views.create_task, name="create_task"),
    path("mine/", views.task_list, name="show_my_tasks"),
]
