from django.shortcuts import render, get_object_or_404
from projects.models import Project
from django.contrib.auth.decorators import login_required
from django.forms import ModelForm
from django.views.generic import FormView


@login_required
def project_list(request):
    projects = Project.objects.filter(owner=request.user)
    return render(request, "projects/list.html", {"projects": projects})


@login_required
def project_detail_view(request, project_id):
    project = get_object_or_404(Project, pk=project_id)
    return render(request, "projects/detail.html", {"project": project})


class ProjectForm(ModelForm):
    class Meta:
        model = Project
        fields = ["name", "description", "owner"]


class CreateProjectView(FormView):
    form_class = ProjectForm
    template_name = "projects/create.html"
    success_url = "/projects/"

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)
