from django.urls import path
from projects import views

urlpatterns = [
    path("", views.project_list, name="list_projects"),
    path("<int:project_id>/", views.project_detail_view, name="show_project"),
    path("create/", views.CreateProjectView.as_view(), name="create_project"),
]
